/*
 * uart.cpp
 *
 *  Created on: 22 ���. 2015
 *      Author: Ievgenii
 */
#include <avr/io.h>
#include "uart.h"


//UART0 initialize
// desired baud rate: 4800
// actual: baud rate:4800 (0,0%)
// char size: 8 bit
// parity: Disabled
void uart0_init(void)
{
 UCSR0B = 0x00; //disable while setting baud rate
 UCSR0A = 0x00;
 UCSR0C = 0x06;
 UBRR0L = 0xBF; //set baud rate lo
 UBRR0H = 0x00; //set baud rate hi
 UCSR0B = 0x18;
 DDRE|=(1<<DDE2);
 PORTE|=(1<<PE2);
}

void uart0_send(uint8_t data){
	while (!(UCSR0A & (1 << UDRE0)));
	UDR0=data;
}

