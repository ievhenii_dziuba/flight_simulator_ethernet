/*
 * leds.cpp
 *
 *  Created on: 21 ���. 2015
 *      Author: Ievgenii
 */

#include "leds.h"
#include <stdint.h>
#include <avr/io.h>
void buttons_init(){
	DDRE&=~(1<<DDE5)|(1<<DDE4)|(1<<DDE3);
	PORTE|=(1<<PE5)|(1<<PE4)|(1<<PE3);
}
void leds_init(){
	DDRF|=(1<<DDF7)|(1<<DDF6)|(1<<DDF5)|(1<<DDF1)|(1<<DDF0);
	PORTF&=~(1<<PF7)|(1<<PF6)|(1<<PF5)|(1<<PF1)|(1<<PF0);
}
void leds(uint8_t led_num, uint8_t color){

	if(led_num==1){
		switch(color){
		case 1:
			PORTF|=(1<<PF6);
			PORTF&=~(1<<PF0);
			break;
		case 2:
			PORTF|=(1<<PF0);
			PORTF&=~(1<<PF6);
			break;
		default:
			PORTF&=~(1<<PF6)|(1<<PF0);
			break;
		}
	}
	else if(led_num==2){
		switch(color){
		case 1:
			PORTF|=(1<<PF5);
			PORTF&=~(1<<PF1);
			break;
		case 2:
			PORTF|=(1<<PF1);
			PORTF&=~(1<<PF5);
			break;
		default:
			PORTF&=~(1<<PF5)|(1<<PF1);
			break;
		}

	}
	else{
		switch(color){
		case 1:
			PORTF|=(1<<PF7);
			break;
		default:
			PORTF&=~(1<<PF7);
			break;
		}
	}
}


