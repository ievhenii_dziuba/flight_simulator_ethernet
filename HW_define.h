/*
 * HW_define.h
 *
 *  Created on: 23 ���. 2015
 *      Author: Ievgenii
 */

#ifndef HW_DEFINE_H_
#define HW_DEFINE_H_

#include <avr/io.h>

#define SCK_PIN 1
#define MOSI_PIN 2
#define MISO_PIN 3

#define SPI_PORT PORTB
#define SPI_DDR DDRB

#define CS_PIN 0

#define CS_PORT PORTB
#define CS_DDR DDRB

#endif /* HW_DEFINE_H_ */
