#ifndef LEDS_H
#define LEDS_H

#include <stdint.h>

void buttons_init();
void leds_init();
void leds(uint8_t led_num, uint8_t color);
#endif
