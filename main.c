/*
 * main.cpp
 *
 *  Created on: 21 ���. 2015
 *      Author: Ievgenii
 */

#include <avr/io.h>
#include <util/delay.h>
#include <stdint.h>
#include "leds.h"
#include "wizchip_conf.h"
#include "w5500/w5500.h"
#include "uart.h"
#include "socket.h"

uint8_t sock;
wiz_NetInfo conf;
uint8_t buff[50];
uint8_t len;

uint8_t UART_TX_BUF[50];
uint8_t UART_TX_BUF_LEN=50;


int main(){

	UART_TX_BUF[0]='a';
	UART_TX_BUF[1]='b';
	UART_TX_BUF[2]='c';
	UART_TX_BUF[3]='d';
	UART_TX_BUF[4]='e';
	UART_TX_BUF[5]='f';
	UART_TX_BUF[6]=0x0D;
	UART_TX_BUF[7]=0x0A;
	UART_TX_BUF_LEN=8;

conf.dhcp=1;
conf.ip[0]=192;
conf.ip[1]=168;
conf.ip[2]=2;
conf.ip[3]=55;
conf.sn[0]=255;
conf.sn[1]=255;
conf.sn[2]=255;
conf.sn[3]=0;
conf.dns[0]=192;
conf.dns[1]=168;
conf.dns[2]=2;
conf.dns[3]=1;
conf.gw[0]=192;
conf.gw[1]=168;
conf.gw[2]=2;
conf.gw[3]=1;
conf.mac[0] = 0xF4; conf.mac[1]=0x60; conf.mac[2] = 0xC9; conf.mac[3]= 0xE9;
conf.mac[4]= 0xB9; conf.mac[5] = 0xCA;

buttons_init();
leds_init();
uart0_init();
SPI_init();
leds(0,0);
leds(1,0);
leds(2,0);

//while(1){
//	if(!(PINE & (1<<PINE3))){
//		leds(1,1);
//	}
//	else leds(1,2);
//}
wizchip_setnetinfo(&conf);
uint8_t memsize[2][8] = { { 2, 2, 2, 2, 2, 2, 2, 2 }, { 2, 2, 2, 2, 2, 2, 2, 2 } };
if (ctlwizchip(CW_INIT_WIZCHIP, (void*) memsize) == -1) {
	uart0_send('E');
	uart0_send('R');
	uart0_send('R');
	uart0_send('O');
	uart0_send('R');
	while (1);
	}
		while (1) {
			switch (getSn_SR(sock)) {

			case SOCK_LISTEN:
				_delay_ms(1000);
				break;

			case SOCK_ESTABLISHED:

				if(!(PINE & (1<<PINE3))){
										leds(1,2);
										send(sock,UART_TX_BUF, UART_TX_BUF_LEN);
										_delay_ms(10);
									}
									else leds(1,0);

				leds(2,1);
				if ((len = getSn_RX_RSR(sock)) > 0) {

					if (len > 50)
						len = 50;
					len = recv(sock, buff, len);

					uint8_t i;
					for(i=0;i<len;i++){
						uart0_send(buff[i]);
					}
				}
				leds(2,2);



				break;

			case SOCK_CLOSE_WAIT:
				disconnect(sock);
				break;

			case SOCK_INIT:
				leds(0,1);
				listen(sock);
				break;

			case SOCK_CLOSED:
			if( !socket(sock,Sn_MR_TCP,2222,0))
					{
					}
					break;

					default:
					break;

				}    // end of switch





			}
		}


